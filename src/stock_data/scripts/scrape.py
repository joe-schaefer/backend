import os
import re
import requests
import urllib.request
import time
import logging

from datetime import datetime

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from stock_data.models import Stock
from stock_data.scripts.progress_bar import progressBar
import stock_data.scripts.loggers as loggers

from django.core.exceptions import ValidationError
from django.conf import settings

from selenium.common.exceptions import NoSuchElementException

from .loggers import logManageStocks


@logManageStocks
def scrape(options):

    # Isolate the provided path
    path = options['scrape']

    # Process the file content
    with open(path, 'r') as f:
        tickers = [ticker.replace('\n', '') for ticker in f.readlines()]
        tickers = [ticker.replace('\t', '') for ticker in tickers]
        tickers = [ticker.replace(' ', '') for ticker in tickers]

    # Generate a list of the symbols for existing stocks
    present_stocks = [stock.symbol
                      for stock
                      in Stock.objects.all()]

    failed_tickers = []

    driver = webdriver.Firefox()

    now = datetime.now()

    # Ouput a progress bar and iterate through each line of the file
    for ticker in progressBar(
            tickers,
            prefix='Scraping',
            length=50
    ):

        # Try to create a stock with the current lines data
        try:

            # If a given ticker not already in database
            if ticker not in present_stocks or options['all']:

                (name, short_description, long_description) = scrapeTicker(
                    driver,
                    ticker
                )

                if ticker not in present_stocks:
                    # Create the stock
                    s = Stock(symbol=ticker)

                else:
                    s = Stock.objects.filter(symbol=ticker)[0]

                s.name = name
                s.short_description = short_description
                s.long_description = long_description

                if name == 'Nasdaq':
                    logger.warning(
                        '{ticker} failed due to google finding nasdaq as the name'.format(
                            ticker=ticker
                        )
                    )

                    os.remove(
                        os.path.join(
                            settings.MEDIA_ROOT,
                            'logos',
                            ticker+'.png'
                        )
                    )
                    continue

                imagePath = os.path.join(
                    'logos',
                    ticker+'.png'
                )

                # if os.path.exists(imagePath):

                s.logo = imagePath

                # Ensure the symbol is valid
                s.full_clean()
                s.save()

                logger.info('{ticker} succeded with name: {name}'.format(
                    ticker=ticker,
                    name=name
                ))

        except (Exception, ValidationError) as e:

            if not logger:
                logger = loggers.getUtilLogger('scrape', logging.WARNING)

            logger.warning(
                '{ticker} failed due to {error_name} {error_message}'.format(
                    ticker=ticker,
                    error_name=type(e).__name__,
                    error_message=str(e).replace('\n', '')
                )
            )

    driver.close()


def scrapeTicker(driver, ticker):

    REFERENCE_PATTERN = r"\[\d*\]"

    driver.get('https://www.google.co.uk/')
    # Find the searchbar
    searchBar = driver.find_element_by_xpath('//*[@title="Search"]')
    searchBar.send_keys('NASDAQ:'+ticker, Keys.RETURN)

    time.sleep(2)

    try:

        # Find the name
        titleDiv = driver.find_element_by_xpath('//*[@data-attrid="title"]')
        name = titleDiv.find_elements_by_tag_name(
            'span')[0].text.replace('\n', '')

        try:
            # Find the short description
            descriptionShortDiv = driver.find_element_by_xpath(
                '//*[@data-attrid="subtitle"]')
            description_short = descriptionShortDiv.find_elements_by_tag_name('span')[
                0].text.replace('\n', '')
        except NoSuchElementException:
            description_short = 'Company'

        # Get image links
        stockContainer = driver.find_element_by_id('wp-tabs-container')
        imageLinks = stockContainer.find_elements_by_tag_name('a')

        # Go to the wiki link
        wikiLink = driver.find_element_by_link_text('Wikipedia')
        wikiLink.click()
        time.sleep(1)

    except NoSuchElementException:

        driver.get('https://www.google.co.uk/')

        # Find the searchbar
        searchBar = driver.find_element_by_xpath('//*[@title="Search"]')
        searchBar.send_keys('NYSE:'+ticker, Keys.RETURN)

        time.sleep(2)

        # Find the name
        titleDiv = driver.find_element_by_xpath('//*[@data-attrid="title"]')
        name = titleDiv.find_elements_by_tag_name(
            'span')[0].text.replace('\n', '')

        try:
            # Find the short description
            descriptionShortDiv = driver.find_element_by_xpath(
                '//*[@data-attrid="subtitle"]')
            description_short = descriptionShortDiv.find_elements_by_tag_name('span')[
                0].text.replace('\n', '')
        except NoSuchElementException:
            description_short = 'Company'

        # Get image links
        stockContainer = driver.find_element_by_id('wp-tabs-container')
        imageLinks = stockContainer.find_elements_by_tag_name('a')

        # Go to the wiki link
        wikiLink = driver.find_element_by_link_text('Wikipedia')
        wikiLink.click()
        time.sleep(1)

    # Find the long description
    wikiBody = driver.find_element_by_class_name('mw-parser-output')
    description_long = [i.text
                        for i
                        in wikiBody.find_elements_by_tag_name('p')
                        if len(i.text) > 50][0]

    for group in re.findall(REFERENCE_PATTERN, description_long):
        description_long = description_long.replace(group, '')

    description_long = description_long.replace('\n', '')

    # Get the logo
    infobox = driver.find_element_by_class_name('infobox')
    images = infobox.find_elements_by_tag_name('img')
    if len(images) > 0:
        src = images[0].get_attribute('src')

        # Save the logo
        urllib.request.urlretrieve(src, os.path.join(
            settings.MEDIA_ROOT,
            'logos',
            ticker+'.png'
        ))

    return (
        name,
        description_short,
        description_long)
