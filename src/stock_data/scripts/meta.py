import numpy as np
import math
import matplotlib.pyplot as plt


from stock_data.models import Stock
from stock_data.scripts.progress_bar import progressBar


def meta(options):
    for stock in progressBar(
            Stock.objects.all(),
            prefix='Generating Metadata',
            length=50
    ):

        series = np.array(stock.getTimeSeries()[1:])
        stock.day_zero_price = series[:-1, -1][0]

        price = series[:-1]/stock.day_zero_price
        volume = series[-1]

        volume[volume == 0] = 1
        volume = np.log(volume)

        stock.mean_price = np.mean(price)
        stock.std_price = np.std(price)

        stock.mean_volume = np.mean(volume)
        stock.std_volume = np.std(volume)

        stock.save()
