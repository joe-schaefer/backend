from stock_data.models import Stock
from stock_data.scripts import sets
from stock_data.network_templates.cat3 import Cat3
import numpy as np
import matplotlib.pyplot as plt


def test(options):
    myCat = Cat3()

    a, b, c, d, e, f = sets.generateSets(
        myCat.filter,
        (100_000, 1_000, 1_000)
    )
    a = a.reshape(a.shape+(1,))

    myCat.fit(
        a,
        b,
        epochs=10,
        validation_data=(e, f)
    )
