import numpy as np


from stock_data.errors import UtilErrors


"""
A helper function to take a single column of data
and normalise it for best interaction with neural networks
"""


def normalise1D(data: list) -> tuple:
    # Ensure the array  is a numpy array
    try:
        data = np.array(data)

    except Exception as e:
        raise TypeError('Input must be array like')

    # If the data provided is not the correct shape
    if len(data.shape) != 1:
        # Raise an error
        raise UtilErrors.ShapeError(
            'The data provided was {} dimentional not 1 dimenstional'.format(
                len(data.shape)
            ))

    # Calculate the median and standard deviation
    median = np.median(data)
    std = np.std(data)

    # Transoform the data
    data -= median
    data = data / std

    # Calculate the minimum and maximum
    maximum = max(data)
    minimum = min(data)

    # Bring within 1 and 0
    data = (data - minimum) / (maximum - minimum)

    # Generate the meta dict
    meta = {'median': median,
            'std': std,
            'max': maximum,
            'min': minimum
            }

    # Return the normalised data and metadata
    return data, meta


"""
A helper function for the normalisation of multiple
data channels whilst maintaining comparability
"""


def normalise2D(data: list) -> tuple:

    # Ensure the data is a numpy array
    try:
        data = np.array(data)

    except Exception as e:
        raise TypeError('Input must be array like')

    # If the data is the wrong shape
    if len(data.shape) != 2:
        # Raise an error
        raise UtilErrors.ShapeError(
            'The data provided was {} dimentional not 2 dimenstional'.format(
                len(data.shape)
            ))

    # Convert the array into a single column
    flat_data = np.array(
        [i for i in data.reshape(data.shape[0] * data.shape[1])])

    # Calculate the median and standard deviation
    # for the entire array
    median = np.median(flat_data)
    std = np.std(flat_data)

    # Transform the data
    flat_data -= median
    flat_data = flat_data / std

    # Calculate the maximum and minimum
    maximum = max(flat_data)
    minimum = min(flat_data)

    # Transform the original dataset using the
    # calculations on the reshaped data
    data -= median
    data = data / std

    data = (data - minimum) / (maximum-minimum)

    # Generate the metadict
    meta = {'median': median,
            'std': std,
            'max': maximum,
            'min': minimum
            }

    return data, meta


"""
Reverses the normalisation of a dataset from the metadict
"""


def reverseNormalisation(data, meta: dict):

    if hasattr(data, '__iter__'):
        data = np.array(data)

    data = (data * (meta['max'] - meta['min'])) + meta['min']
    data = data * meta['std']
    data += meta['median']

    return data
