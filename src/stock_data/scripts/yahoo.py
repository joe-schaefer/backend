import numpy

from yahoo_fin import stock_info as si

"""
A function to retrieve the currrent price of a ticker
from yahoo finance
"""


def getRecentPrice(ticker: str) -> float:
    return si.get_live_price(ticker)


"""
A function to retirive the currrent price of multiple
tickers from yahoo finance
"""


def getRecentPrices(tickers: list) -> list:
    return np.array([si.get_live_price(ticker) for ticker in tickers])
