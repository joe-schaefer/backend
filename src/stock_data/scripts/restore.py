import os
import logging
from datetime import datetime

from stock_data.models import Stock
from stock_data.scripts.delete import delete
from stock_data.scripts.progress_bar import progressBar
from stock_data.scripts import loggers

from .loggers import logManageStocks

from django.conf import settings


@logManageStocks
def restore(options):

    now = datetime.now()
    last = None

    backups_dir = os.path.join(
        settings.BASE_DIR,
        'stock_data',
        'backups'
    )

    for backup in os.listdir(backups_dir):
        backup = datetime.strptime(
            os.path.splitext(backup)[0],
            '%d-%m-%Y %H:%M:%S'
        )

        if not last:
            last = backup

        elif (backup-last).total_seconds() > 0.0:
            last = backup

    print('Most recent backup:', last.strftime('%d-%m-%Y %H:%M:%S'))
    last = last.strftime('%d-%m-%Y %H:%M:%S') + '.txt'

    with open(os.path.join(
            backups_dir,
            last), 'r') as f:

        lines = [line.replace('\n', '').split('\t') for line in f.readlines()]

        for i, line in enumerate(progressBar(
            lines,
            prefix='Restoring',
            length=50
        )):

            try:
                s = Stock(
                    symbol=line[0],
                    name=line[1],
                    short_description=line[2],
                    long_description=line[3],
                    logo=os.path.join(
                        'logos',
                        line[0]+'.png'
                    )
                )
                s.full_clean()
                s.save()

            except Exception as e:

                if not logger:
                    logger = loggers.getUtilLogger('restore', logging.WARNING)

                logger.warning(
                    '{ticker} on line {line} failed due to {error_name} {error_message}'.format(
                        ticker=line[0],
                        line=i,
                        error_name=type(e).__name__,
                        error_message=str(e).replace('\n', '')
                    )
                )
