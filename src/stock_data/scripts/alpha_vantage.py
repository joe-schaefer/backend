import json
import requests
import numpy as np

from itertools import cycle
from stock_data.errors import ApiErrors


class ApiHandle:
    """
    A class to handle interactions with the the alpha vantage api.
    """

    def __init__(self):
        self.keys = None

    """
    Load api keys from the specififed file. The files is expected to be
    plain text, with each key occupying its own line.
    """

    def loadKeys(self, path: str):
        with open(path) as f:
            self.keys = cycle(f.readlines())

    """
    Returns stock data retirived from the alpha vantage api.
    For example request(foo='bar') would attempt to read and clean data from
    https://www.alphavantage.co/query?foo=bar&key=*******&.
    See alpha www.alphavantage.co for valid api calls.

    A typical  return would be:

        ([
            ['2020-06-29 14:28:00', 119.12, 119.12,
            119.02, 119.09, 5861.0],
            ['2020-06-29 14:27:00', 119.19, 119.19,
            119.095, 119.11, 2618.0]
        ],

        ['2020-06-29 16:00:00', '1. open', '2. high',
        '3. low', '4. close', '5. volume']
        )

    """

    def request(self, **kwargs) -> tuple:

        # If keys not provided, raise error
        if not self.keys:
            raise ApiErrors.NoKeysLoaded(
                'No Api keys were provided, please call this.loadKeys to \
                load the keys. See https: // www.alphavantage.co to \
                request a key')

        # Start the request string
        request = 'https://www.alphavantage.co/query?'

        # For each argument, append arg=value to the string
        for arg in kwargs:
            request += arg + '=' + kwargs[arg] + '&'

        # Append the required api key info and step through to the next key
        request += 'apikey=' + next(self.keys)

        # Present the request for debugging
        # print('Request: '+request)

        # Get the responce from the generated request url
        responce = requests.get(request)

        # If something went wrong
        if responce.status_code != 200:
            # Raise an error
            raise ApiErrors.RequestFailed(
                'Alpha vantage api request failed with: {}'.format(
                    resp.status_code
                )
            )

        # Get the responce as a dictionary
        data = responce.json()

        # Get the keys of the dictionary
        data_keys = data.keys()

        # Ensures data is recieved
        if not data:
            raise ApiErrors.EmptyResponce(
                'Data must not be None'
            )

        # Ensures data has keys to call
        elif len(data_keys) == 0:
            raise ApiErrors.EmptyResponce('Responce is dict with no keys')

        # Ensures no error message
        elif 'Error Message' in data_keys:
            raise ApiErrors.InvalidRequest(data['Error Message'])

        # Ensures no note
        elif 'Note' in data_keys:
            raise ApiErrors.InvalidRequest(data['Note'])

        # Rettrieves the date at which the data was last refreshed
        meta = [data['Meta Data']['3. Last Refreshed']]

        # Isolates the series from the overall dictionary
        series = list(data.values())[1]

        # Extracts the type of data generated
        meta += list(list(series.values())[0].keys())
        meta = np.array(meta)

        # Generates the dataset from the array of dictionarys
        data = [[key] + [float(i)
                         for i
                         in list(series[key].values())
                         ]
                for key
                in series.keys()
                ]

        day_zero = data[-1]

        if not False in [i == 0 for i in day_zero[1:]]:
            del data[-1]
            print('Fak yea__________________')

        # Returns the dataset and metadata
        return data, meta
