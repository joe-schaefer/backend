import os
import time
import logging
from datetime import datetime

from stock_data.models import Stock
from stock_data.scripts.progress_bar import progressBar

from stock_data.scripts.alpha_vantage import ApiHandle
from stock_data.errors import ApiErrors
from stock_data.scripts import loggers

from .loggers import logManageStocks


@logManageStocks
def update(options):

    now = datetime.now()
    # Create an api handle and load its keys
    myHandle = ApiHandle()

    myHandle.loadKeys(os.path.join(
        'stock_data',
        'static',
        'premium_key.txt'
    ))

    for stock in progressBar(
        Stock.objects.all(),
        prefix='Updating',
        length=50
    ):
        try:
            data, meta = myHandle.request(
                symbol=stock.symbol,
                function='TIME_SERIES_DAILY',
                outputsize='full'
            )
            if stock.last_updated is not None:
                if meta[0] == stock.last_updated.strftime("%Y-%m-%d"):
                    continue

            stock.setTimeSeries(data)

            try:
                stock.last_updated = datetime.strptime(
                    meta[0],
                    '%Y-%m-%d').date()

            except ValueError:
                stock.last_updated = datetime.strptime(
                    meta[0],
                    '%Y-%m-%d %H:%M:%S').date()

            stock.save()

        except (ApiErrors.EmptyResponce,
                ApiErrors.RequestFailed,
                ApiErrors.InvalidRequest,
                NameError) as e:

            if not 'logger' in locals():
                logger = logger = loggers.getUtilLogger(
                    'update',
                    logging.WARNING
                )

            logger.warning(stock.symbol+' '+type(e).__name__+' '+str(e))
