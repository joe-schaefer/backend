from stock_data.models import Stock
from stock_data.scripts.progress_bar import progressBar
from .loggers import logManageStocks


@logManageStocks
def group(options):
    for i, stock in enumerate(progressBar(
        Stock.objects.all(),
        prefix='Grouping',
        length=50
    )):
        if i % 10 >= 0 and i % 10 < 7:
            stock.learning_use = Stock.TRAIN
        elif i % 10 == 7 or i % 10 == 8:
            stock.learning_use = Stock.TEST
        elif i % 10 == 9:
            stock.learning_use = Stock.VALIDATE
        stock.save()
