import os
import logging
import functools

from datetime import datetime

from django.conf import settings

stocksLogger = logging.getLogger('Stocks')
stocksLogger.setLevel(logging.INFO)

logFileHandler = logging.FileHandler(
    os.path.join(
        settings.BASE_DIR,
        'stock_data',
        'management',
        'logs',
        '!stocks.log'
    )
)

logFileFormattter = logging.Formatter(
    '%(asctime)s - %(levelname)s - %(message)s'
)

logFileHandler.setFormatter(logFileFormattter)

stocksLogger.addHandler(logFileHandler)


def logManageStocks(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            stocksLogger.info(func.__name__)
            res = func(*args, **kwargs)

        except Exception as e:
            stocksLogger.warning(
                '{func} {error} {message}'.format(
                    func=func.__name__,
                    error=type(e).__name__,
                    message=str(e)
                )
            )

    return wrapper


def getUtilLogger(name, level):

    now = datetime.now()

    logger = logging.getLogger(name)
    logger.setLevel(level)

    logFileHandler = logging.FileHandler(
        os.path.join(
            settings.BASE_DIR,
            'stock_data',
            'management',
            'logs',
            name+' '+now.strftime('%d-%m-%Y %H:%M:%S')+'.log'
        )
    )

    logFileFormattter = logging.Formatter(
        '%(asctime)s - %(levelname)s - %(message)s'
    )

    logFileHandler.setFormatter(logFileFormattter)

    logger.addHandler(logFileHandler)

    return logger
