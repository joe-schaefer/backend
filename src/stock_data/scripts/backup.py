import os

from .loggers import logManageStocks

from datetime import datetime
from django.conf import settings

from stock_data.models import Stock
from stock_data.scripts.progress_bar import progressBar


@logManageStocks
def backup(options):
    now = datetime.now()
    backups_dir = os.path.join(
        settings.BASE_DIR,
        'stock_data',
        'backups'
    )

    out = []
    for stock in progressBar(
        Stock.objects.all(),
        prefix='Backing up',
        length=50
    ):
        out.append('\t'.join([
            stock.symbol,
            stock.name,
            stock.short_description,
            stock.long_description])
        )

    with open(os.path.join(
        'stock_data',
        'backups',
        now.strftime('%d-%m-%Y %H:%M:%S')+'.txt'
    ), 'w'
    ) as f:
        f.write('\n'.join(out))
