import numpy as np

from stock_data.models import Stock
from stock_data.errors import SetErrors


def __getQuotas(
        array_length: int,
        target_sum: int) -> list:

    # Define the output list
    quotas = [0]*array_length

    # Determine quotas recursivly
    while sum(quotas) != target_sum:

        # Get the current difference between the total of the quotas and
        # desired size
        quotas_sum = sum(quotas)

        # If the required increase is more than 1 for every ticker
        if target_sum - quotas_sum > array_length:

            increment = int((target_sum - quotas_sum) / array_length)
            quotas = [i+increment for i in quotas]

        else:
            # Add 1 to enough quotas to bring the sum of quotas to equal
            # target size
            for x in range(0, target_sum - quotas_sum):
                quotas[x] += 1

    quotas = np.array(quotas)
    np.random.shuffle(quotas)
    return [i for i in quotas]


"""
Returns a train,test,and val acivations and labels using a
filter class and a load function that returns the stockdata
to be used from a ticker
"""


def generateSets(
        fltr,
        sizes: tuple = (10_000, 1_000, 1_000)) -> tuple:

    output = ()
    learning_types = (Stock.TRAIN, Stock.TEST, Stock.VALIDATE)

    for size, learning_type in zip(sizes, learning_types):

        # Generate the activations per ticker to be generated
        stocks = Stock.objects.filter(learning_use=learning_type)
        print(learning_type, len(stocks))
        quotas = __getQuotas(len(stocks), size)

        excess = None

        activations = []
        labels = []

        for stock, quota in zip(stocks, quotas):

            series = stock.getProcessedSeries()

            # Generate a list of unique start indicies
            available_days = list(
                range(0,
                      series.shape[-1]
                      - fltr.activation_length
                      - fltr.future_index
                      )
            )

            # If the previous ticker couldnot furfill the quota
            if excess is not None:

                # Add the previoustickers shortcommings to this quota
                quota += excess

                # Reset the excess
                excess = None

            # If unable to meet the current quota
            if len(available_days) < quota:

                # Store the deficit for the next ticker
                excess = quota - len(available_days)

                # Settle for what is possible
                quota = len(available_days)

            # Shuffle the possible start indicies
            np.random.shuffle(available_days)

            for day in available_days[:quota]:
                l = series.shape[-1]

                try:

                    activation = fltr.genActivation(
                        series[:, :l-day],
                        stock
                    )

                    label = fltr.genLabel(
                        series[:, :l-day],
                        stock
                    )

                    if activation is None or label is None:
                        raise SetErrors.FilterReturnedNone

                    activations.append(activation)
                    labels.append(label)

                except AttributeError:
                    raise SetErrors.InvalidFilter(
                        'Filter is missing essential functions genActivation \
                            or genLabel')

                except SetErrors.FilterReturnedNone:
                    raise SetErrors.InvalidFilter(
                        'Either Filter.genActivations  or Filtter.genLabels \
                            returned None')

        output += (np.array(activations), np.array(labels))

    return output
