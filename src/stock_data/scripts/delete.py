from stock_data.models import Stock
from stock_data.scripts.progress_bar import progressBar
from .loggers import logManageStocks


@logManageStocks
def delete(options):
    if len(Stock.objects.all()) != 0:
        for i, stock in enumerate(progressBar(
            Stock.objects.all(),
            prefix='Deleting',
            length=50
        )):
            """
            stock.time_series = None
            stock.last_updated = None
            stock.save()

            """
            stock.delete()
