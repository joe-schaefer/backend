import tensorflow as tf
import tensorflow.keras as keras

from stock_data.filters import LinCatagorisation


class Cat3(keras.Model):
    def __init__(self):
        super(Cat3, self).__init__()
        self.filter = LinCatagorisation(0.009)

        self.conv1 = keras.layers.Conv2D(
            144,
            (1, 1),
            activation='sigmoid',
            input_shape=(None, 5, 196, 1)
        )

        self.conv2 = keras.layers.Conv2D(
            256,
            (3, 3),
            activation='sigmoid'
        )

        self.flat = keras.layers.Flatten()

        self.dense1 = keras.layers.Dense(700)
        self.sigmoid1 = keras.layers.Activation(tf.nn.sigmoid)

        self.dense2 = keras.layers.Dense(700)
        self.sigmoid2 = keras.layers.Activation(tf.nn.sigmoid)

        self.dense3 = keras.layers.Dense(700)
        self.sigmoid3 = keras.layers.Activation(tf.nn.sigmoid)

        self.dense4 = keras.layers.Dense(700)
        self.sigmoid4 = keras.layers.Activation(tf.nn.sigmoid)

        self.concat = keras.layers.concatenate

        self.out = keras.layers.Dense(self.filter.catagories)
        self.softmax = keras.layers.Activation(tf.nn.softmax)

        self.learningScedule = tf.keras.optimizers.schedules.ExponentialDecay(
            0.000001,
            decay_steps=100000,
            decay_rate=0.96,
            staircase=True)

        self.compile(
            optimizer=keras.optimizers.Adam(
                learning_rate=self.learningScedule
            ),
            loss=keras.losses.CategoricalCrossentropy(),
            metrics=['accuracy']
        )

    @staticmethod
    def loadModel(path: str):
        model = tf.keras.models.load_model(path)
        model.filter = LinCatagorisation(0.009)
        return model

    def saveModel(self, path):
        del self.filter
        self.save(path)

    def call(self, inputs):

        flatInputs = x = self.flat(inputs)

        x = self.conv1(inputs)
        x = self.conv2(x)

        x = self.flat(x)

        x = self.concat([x, flatInputs])

        x = self.dense1(x)
        x = self.sigmoid1(x)

        x = self.dense2(x)
        x = self.sigmoid2(x)

        x = self.dense3(x)
        x = self.sigmoid3(x)

        x = self.dense4(x)
        x = self.sigmoid4(x)

        x = self.out(x)

        return x
