import tensorflow as tf
import tensorflow.keras as keras

from stock_data.filters import LinCatagorisation


class Cat1(keras.Model):
    def __init__(self):
        super(Cat1, self).__init__()

        self.filter = LinCatagorisation(0.009)

        self.flatten = keras.layers.Flatten(
            input_shape=(None, 5, self.filter.activation_length))

        self.dense1 = keras.layers.Dense(1000)
        self.sigmoid1 = keras.layers.Activation(tf.nn.sigmoid)
        self.dropout1 = keras.layers.Dropout(0.2)

        self.dense2 = keras.layers.Dense(1000)
        self.sigmoid2 = keras.layers.Activation(tf.nn.sigmoid)
        self.dropout2 = keras.layers.Dropout(0.2)

        self.dense3 = keras.layers.Dense(1000)
        self.sigmoid3 = keras.layers.Activation(tf.nn.sigmoid)
        self.dropout3 = keras.layers.Dropout(0.2)

        self.dense4 = keras.layers.Dense(1000)
        self.sigmoid4 = keras.layers.Activation(tf.nn.sigmoid)
        self.dropout4 = keras.layers.Dropout(0.2)

        self.dense5 = keras.layers.Dense(1000)
        self.sigmoid5 = keras.layers.Activation(tf.nn.sigmoid)
        self.dropout5 = keras.layers.Dropout(0.2)

        self.dense6 = keras.layers.Dense(1000)
        self.sigmoid6 = keras.layers.Activation(tf.nn.sigmoid)
        self.dropout6 = keras.layers.Dropout(0.2)

        self.out = keras.layers.Dense(self.filter.catagories)
        self.sigmoid7 = keras.layers.Activation(tf.nn.sigmoid)

        self.compile(optimizer=keras.optimizers.Adam(learning_rate=0.00001),
                     loss=keras.losses.CategoricalCrossentropy(),
                     metrics=['accuracy'])

    @staticmethod
    def loadModel(path: str):
        model = tf.keras.models.load_model(path)
        model.filter = LinCatagorisation(0.009)
        return model

    def saveModel(self, path):
        del self.filter
        self.save(path)

    def call(self, inputs):

        x = self.flatten(inputs)

        x = self.dense1(x)
        x = self.sigmoid1(x)
        x = self.dropout1(x)

        x = self.dense2(x)
        x = self.sigmoid2(x)
        x = self.dropout2(x)

        x = self.dense3(x)
        x = self.sigmoid3(x)
        x = self.dropout3(x)

        x = self.dense4(x)
        x = self.sigmoid4(x)
        x = self.dropout4(x)

        x = self.dense5(x)
        x = self.sigmoid5(x)
        x = self.dropout5(x)

        x = self.dense6(x)
        x = self.sigmoid6(x)
        x = self.dropout6(x)

        x = self.out(x)
        x = self.sigmoid7(x)

        return x
