import os

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from stock_data.scripts.yahoo import getRecentPrice
from stock_data.scripts.alpha_vantage import ApiHandle

myHandle = ApiHandle()
myHandle.loadKeys(os.path.join(
    'stock_data',
    'static',
    'api_keys.txt')
)


def validate_ticker(ticker):

    try:
        getRecentPrice(ticker)

    except:
        try:
            myHandle.request(unction='TIME_SERIES_DAILY', symbol=ticker)

        except:
            raise ValidationError(_(
                '{} does not exist within yahoo or alpha vantages databases'.format(ticker)))
