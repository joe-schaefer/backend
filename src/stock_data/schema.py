import graphene
import json
from graphene_django.types import DjangoObjectType

from stock_data.models import Stock


class StockType(DjangoObjectType):
    class Meta:
        model = Stock

    def resolve_time_series(parent, info):
        return json.loads(parent.time_series)


class Query:

    stock = graphene.Field(
        StockType,
        symbol=graphene.String(),
        training_use=graphene.String()

    )

    stocks = graphene.List(
        StockType,
        learning_use=graphene.String(),
        last_updated=graphene.Date()
    )

    all_stocks = graphene.List(StockType)

    def resolve_stock(self, info, **kwargs):
        symbol = kwargs.get('symbol')
        if symbol is not None:
            return Stock.objects.get(symbol=symbol)

    def resolve_stocks(self, info, **kwargs):
        learning_use = kwargs.get('learning_use')
        last_updated = kwargs.get('last_updated')

        if learning_use and not last_updated:
            return Stock.objects.filter(learning_use=learning_use)

        elif not learning_use and last_updated:
            return Stock.objects.filter(last_updated=last_updated)

        elif learning_use and last_updated:

            return Stock.objects.filter(
                last_updated=last_updated,
                learning_use=learning_use
            )

    def resolve_all_stocks(self, info, **kwargs):
        # We can easily optimize query count in the resolve method
        return Stock.objects.all()
