class ApiErrors:
    """
    Errors for use in api submodules
    """

    class NoKeysLoaded(BaseException):
        """
        An error to notify that no api keys have been loaded
        """
        pass

    class EmptyResponce(BaseException):
        """
        An error indicating that the api's responce returned no data
        """
        pass

    class RequestFailed(BaseException):
        """
        An error indicating that the api request failed
        """
        pass

    class InvalidRequest(BaseException):
        """
        An error indication that the request returned with a note that
        the request was invalid
        """
        pass


class UtilErrors:
    """
    Errors to be called by the utils submodule
    """
    class ShapeError(BaseException):
        """
        An error indicating that the shape of a given input is invalid
        """
        pass


class SetErrors:
    class InvalidFilter(BaseException):
        pass

    class FilterReturnedNone(BaseException):
        pass
