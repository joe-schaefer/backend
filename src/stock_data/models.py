import os
import json
import numpy as np


from django.db import models
from django.conf import settings
from .validators import validate_ticker
from .scripts import tools
import matplotlib.pyplot as plt


class Stock(models.Model):

    symbol = models.CharField(
        max_length=10,
        primary_key=True,
        unique=True,
        db_index=True,
        validators=[validate_ticker]
    )

    name = models.CharField(
        max_length=50,
        blank=True
    )

    short_description = models.CharField(
        max_length=100,
        blank=True

    )

    long_description = models.TextField(
        max_length=1000,
        null=True,
        blank=True
    )

    time_series = models.TextField(
        editable=False,
        blank=True,
        null=True
    )

    TEST = 'TE'
    TRAIN = 'TR'
    VALIDATE = 'VA'

    learning_use_choices = [
        (TEST, 'Testing'),
        (TRAIN, 'Training'),
        (VALIDATE, 'Validation')
    ]

    learning_use = models.CharField(
        max_length=2,
        choices=learning_use_choices,
        null=True,
        editable=False,
        default=TRAIN
    )

    logo = models.ImageField(
        upload_to='logos/',
        blank=True
    )

    last_updated = models.DateField(
        null=True,
        editable=False,
        blank=True
    )

    mean_price = models.FloatField(
        null=True,
        editable=False,
        blank=True
    )

    std_price = models.FloatField(
        null=True,
        editable=False,
        blank=True
    )

    mean_volume = models.FloatField(
        null=True,
        editable=False,
        blank=True
    )

    std_volume = models.FloatField(
        null=True,
        editable=False,
        blank=True
    )

    day_zero_price = models.FloatField(
        null=True,
        editable=False,
        blank=True
    )

    def __str__(self):
        return self.symbol

    def getTimeSeries(self):
        if self.time_series:
            return json.loads(self.time_series)

    def setTimeSeries(self, series: list):
        self.time_series = json.dumps(series)

    def getProcessedSeries(self):

        series = np.array(self.getTimeSeries()[1:])

        price = series[:-1]
        volume = series[-1]

        price = price/self.day_zero_price
        volume[volume == 0] = 1
        volume = np.log(volume)

        price = (price-self.mean_price)/self.std_price
        volume = (volume-self.mean_volume)/self.std_volume

        return np.concatenate(
            (
                price,
                volume.reshape(1, len(volume))
            )
        )
