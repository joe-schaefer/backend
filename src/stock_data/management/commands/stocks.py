import os
import time
import logging

import stock_data.scripts.loggers as loggers

from stock_data.scripts.delete import delete
from stock_data.scripts.group import group
from stock_data.scripts.scrape import scrape
from stock_data.scripts.update import update
from stock_data.scripts.backup import backup
from stock_data.scripts.restore import restore
from stock_data.scripts.test import test
from stock_data.scripts.meta import meta

from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings


class Command(BaseCommand):

    help = 'Provides functionality for the maintanance of stocks and their data'

    def add_arguments(self, parser):

        grp = parser.add_mutually_exclusive_group(required=False)

        parser.add_argument(
            '-s',
            '--scrape',
            help='Scrapes stock data using tickers from a text file and uses them to scrape necessary info from the web',
            type=str
        )

        parser.add_argument(
            '-u',
            '--update',
            help='Loads an altered but unprocessed/non-normalised time series for each stock using the alpha vantage api',
            action="store_true",
            default=False
        )

        parser.add_argument(
            '-g',
            '--group',
            help='Sets the learning use of each stock in a (8:1:1, TR,TE,VA) distribution',
            action='store_true',
            default=False
        )

        parser.add_argument(
            '-d',
            '--delete',
            help='Deltes all existing stocks and stock data',
            action='store_true',
            default=False
        )

        parser.add_argument(
            '-a',
            '--all',
            help='A flag to cause all stocks to be updated rather than just missing stocks',
            action='store_true',
            default=False

        )

        grp.add_argument(
            '-b',
            '--backup',
            help='Creates a backup of the current stocks and their permenant data',
            action='store_true',
            default=False
        )

        grp.add_argument(
            '-r',
            '--restore',
            help='Restores from the latest backup',
            action='store_true',
            default=False
        )

        parser.add_argument(
            '-t',
            '--test',
            help='Runs the test script',
            action='store_true',
            default=False
        )

        parser.add_argument(
            '-m',
            '--meta',
            help='Generates the meta data for consistant normalisation of each stocks time series',
            action='store_true',
            default=False
        )

    def handle(self, *args, **options):
        """
        Runs one of multiple utils based on inputs
        """
        # Imports stock info
        if options['scrape']:
            scrape(options)

        # Loads, cleans, and serialises time series data
        if options['update']:
            update(options)

        if options['group']:
            group(options)

        if options['delete']:
            delete(options)

        if options['backup']:
            backup(options)

        if options['restore']:
            delete(options)
            restore(options)
            update(options)

        if options['test']:
            test(options)

        if options['meta']:
            meta(options)
