import numpy as np
import tensorflow as tf

from stock_data.scripts.tools import reverseNormalisation


class LinCatagorisation:
    """
    A default filter for catagorisation networks
    """

    def __init__(self, tolerence: float):
        # Defines activation length
        self.activation_length = 14**2

        self.future_index = 1

        # Defines the number of catagories
        self.catagories = 3

        # Defines the upper and lower bounds of the catagorisation range
        self.uBound = 1 + tolerence
        self.lBound = 1 - tolerence

        # Defines labels
        self.labels = tf.one_hot(
            [i for i in range(0, self.catagories)], self.catagories).numpy()

        # Prealculate the bounds
        self.bounds = [self.lBound + x*(self.uBound-self.lBound)/(
            self.catagories-2) for x in range(0, self.catagories-1)]

    """
    Defines the core behaviour of the filter by generating a lable
    from a given input
    """

    def genLabel(self, series: list, stock) -> list:

        a = series[3][-(self.activation_length+self.future_index)]
        b = series[3][-self.activation_length]

        a = (a * stock.std_price) + stock.mean_price
        a = a * stock.day_zero_price

        b = (b * stock.std_price) + stock.mean_price
        b = b * stock.day_zero_price

        # Calculate the relative change
        change = a/b

        # Determine and return the apropriate lable

        if change <= self.bounds[0]:
            return self.labels[0]
            # return 0

        elif change > self.bounds[-1]:
            return self.labels[-1]
            # return self.catagories - 1

        else:
            for x in range(1, len(self.bounds)+1):
                if change > self.bounds[x-1] and change <= self.bounds[x]:
                    return self.labels[x]
                    # return x

    def genActivation(self, series: list, stock) -> list:
        return series[:, -self.activation_length:]


class Regression:
    """
    Defines a class for use by a regression network
    """

    def __init__(self):
        self.activation_length = 14**2

    def genLabel(self, series: list) -> float:
        return series[4][self.activation_length]

    def genActivation(self, series: list) -> float:
        return [i[:self.activation_length] for i in series]
